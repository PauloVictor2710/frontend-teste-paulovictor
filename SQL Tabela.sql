CREATE DATABASE facedigital;
 
 
CREATE TABLE usuario
(
	nome   VARCHAR(100) NOT NULL,
    email   VARCHAR(100) NOT NULL,
    senha   VARCHAR(100) NOT NULL,
    cnpj   VARCHAR(100) NOT NULL,
    razao   VARCHAR(100) NOT NULL,
    nomefan   VARCHAR(100) NOT NULL,
    site   VARCHAR(100) NOT NULL,
    telefone   VARCHAR(100) NOT NULL
);